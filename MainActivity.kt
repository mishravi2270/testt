package com.example.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.test.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding  //defining the binding class

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setRecyclerView()
    }

    private fun setRecyclerView() {
        // first rv
        val searchedData = ArrayList<CartData>()
        binding.rvRowSearched.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        for (i in 1..10) {
            searchedData.add(
                CartData(
                    "",
                    "Save this product",
                    "In Stock",
                    7.99f,
                    "Screw Driver",
                    4f
                )
            )
        }
        val searchedAdapter = ItemAdapter(searchedData)
        binding.rvRowSearched.adapter = searchedAdapter

// secocond rv
        val suggestedData = ArrayList<CartData>()
        binding.rvRowSuggested.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)

        for (i in 1..10) {
            suggestedData.add(
                CartData(
                    "",
                    "Locate this product",
                    "In Stock",
                    7.99f,
                    "Screw Driver",
                    4f
                )
            )
        }
        val suggestedAdapter = ItemAdapter(searchedData)
        binding.rvRowSuggested.adapter = suggestedAdapter
    }
}